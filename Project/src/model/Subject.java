package model;

import java.io.Serializable;

public class Subject implements Serializable{
	private int id;
	private String createDate;
	private int subjectId;
	private int money;
	private String memo;
	private int typeId;
	private String subjectName;
	private int totalMoney;


	public Subject(String subjectName) {
		this.subjectName=subjectName;

	}


	public Subject(String subjectName,int money,String memo) {
		this.memo=memo;
		this.money=money;
		this.subjectName=subjectName;
	}


	public Subject(int subId,String createDate,String subjectName,int money,String memo) {
		this.id=subId;
		this.createDate=createDate;
		this.memo=memo;
		this.money=money;
		this.subjectName=subjectName;
	}


	public Subject(int subId,String subjectName,int money,String memo) {
		this.id=subId;
		this.memo=memo;
		this.money=money;
		this.subjectName=subjectName;
	}


	public Subject(int id,int subjectId,int typeId, int money,String memo) {
		this.id=id;
		this.subjectId=subjectId;
		this.typeId=typeId;
		this.money=money;
		this.memo=memo;
	}

	public Subject(int subjectId, String subjectName) {
		this.subjectId=subjectId;
		this.subjectName=subjectName;

	}

	public String getFormatTotalPrice() {
		return String.format("%,d", this.totalMoney);
	}


	public int getTotalMoney() {
		return totalMoney;
	}


	public void setTotalMoney(int totalMoney) {
		this.totalMoney = totalMoney;
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}

	public String getCreateDate() {
		return createDate;
	}
	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}
	public int getSubjectId() {
		return subjectId;
	}
	public void setSubjectId(int subjectId) {
		this.subjectId = subjectId;
	}
	public int getMoney() {
		return money;
	}
	public void setMoney(int money) {
		this.money = money;
	}
	public String getMemo() {
		return memo;
	}
	public void setMemo(String memo) {
		this.memo = memo;
	}
	public int getTypeId() {
		return typeId;
	}
	public void setTypeId(int typeId) {
		this.typeId = typeId;
	}
	public String getSubjectName() {
		return subjectName;
	}
	public void setSubjectName(String subjectName) {
		this.subjectName = subjectName;
	}


}
