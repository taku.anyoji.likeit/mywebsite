package model;

public class ParseInt {

	public static int parseInt(String value) {

		try {
	        // intに変換して返す
	        return Integer.parseInt(value);
	    } catch ( NumberFormatException e ) {
	        // デフォルト値を返す
	        return 0;
	    }
	}

}
