package model;

import java.io.Serializable;

public class CalemdarData implements Serializable {
	private int date;
	private int totalEx;
	private int totalIn;


	public int getDate() {
		return date;
	}
	public void setDate(int date) {
		this.date = date;
	}
	public int getTotalIn() {
		return totalIn;
	}
	public void setTotalIn(int totalIn) {
		this.totalIn = totalIn;
	}

	public int getTotalEx() {
		return totalEx;
	}
	public void setTotalEx(int totalEx) {
		this.totalEx = totalEx;
	}
}
