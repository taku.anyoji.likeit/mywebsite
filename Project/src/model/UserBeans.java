package model;

import java.io.Serializable;

public class UserBeans implements Serializable{

	private int userId;
	private String loginId;
	private String userName;
	private String birthDate;
	private String password;


	public UserBeans() {

	}

	public UserBeans(int userIdDate,String loginIdDate, String nameDate,String birthDate) {
		this.userId=userIdDate;
		this.loginId=loginIdDate;
		this.userName=nameDate;
		this.birthDate=birthDate;

	}

	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public String getLoginId() {
		return loginId;
	}
	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getBirthDate() {
		return birthDate;
	}
	public void setBirthDate(String birthDate) {
		this.birthDate = birthDate;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}

}
