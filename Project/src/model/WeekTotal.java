package model;

import java.io.Serializable;

public class WeekTotal implements Serializable {

	private int weekTotalEx;
	private int weekTotalIn;


	public int getWeekTotalEx() {
		return weekTotalEx;
	}

	public void setWeekTotalEx(int weekTotalEx) {
		this.weekTotalEx = weekTotalEx;
	}
	public int getWeekTotalIn() {
		return weekTotalIn;
	}
	public void setWeekTotalIn(int weekTotalIn) {
		this.weekTotalIn = weekTotalIn;
	}

}
