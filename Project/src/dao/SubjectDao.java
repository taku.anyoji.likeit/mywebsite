package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import model.Subject;

public class SubjectDao {

	//科目名000取得メソッド（SELECTタグ用）
	public List<Subject> subject(int typeId) {

		Connection conn = null;
		List<Subject> subList = new ArrayList<Subject>();
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			//  ここに処理を書いていく
			String sql = "SELECT * FROM subject WHERE type_id = ?";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setInt(1, typeId);
			ResultSet rs = pStmt.executeQuery();

			while (rs.next()) {
			int subjectId = rs.getInt("subject_id");
			String subjectName = rs.getString("subject_name");
			Subject sub=new Subject(subjectId,subjectName);
			subList.add(sub);
			}
			return subList;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	//科目登録メソッド
		public int insert(int inUsId,String inDate,int inRegist,int inMoney,String inMemo) {

			Connection conn=null;

			try {
				conn=DBManager.getConnection();

				String sql="INSERT INTO household(user_id,create_date,subject_id,money,memo) "
						+ "VALUES(?,?,?,?,?);";
				PreparedStatement pStmt = conn.prepareStatement(sql);
				pStmt.setInt(1, inUsId);
				pStmt.setString(2, inDate);
				pStmt.setInt(3, inRegist);
				pStmt.setInt(4, inMoney);
				pStmt.setString(5, inMemo);

				int rs=pStmt.executeUpdate();
				return rs;
			} catch (SQLException e) {
				e.printStackTrace();
				return 0;
			} finally {
				// データベース切断
				if (conn != null) {
					try {
						conn.close();
					} catch (SQLException e) {
						e.printStackTrace();
						return 0;
					}
				}
			}
		}

		//日付詳細データ取得メソッド

		public List<Subject> dayDetail(String date,int userId){

			Connection conn = null;
			List<Subject> dayList = new ArrayList<Subject>();
			try {
				// データベースへ接続
				conn = DBManager.getConnection();

//				  ここに処理を書いていく
				String sql = " select" +
						" household.id," +
						" subject.subject_name," +
						" household.money," +
						" household.create_date," +
						" household.id,"+
						" household.memo" +
						" from" +
						"  subject right outer join household" +
						"  on" +
						"  household.subject_id=subject.subject_id" +
						"  where" +
						"  household.create_date=?" +
						"  and" +
						"  user_id=?";

				PreparedStatement pStmt = conn.prepareStatement(sql);
				pStmt.setString(1, date);
				pStmt.setInt(2, userId);
				ResultSet rs = pStmt.executeQuery();

				while (rs.next()) {
				int subId = rs.getInt("id");
				String subjectName = rs.getString("subject_name");
				int money = rs.getInt("money");
				String memo= rs.getString("memo");
				Subject sub=new Subject(subId,subjectName,money,memo);
				dayList.add(sub);
				}
				return dayList;
			} catch (SQLException e) {
				e.printStackTrace();
				return null;
			} finally {
				// データベース切断
				if (conn != null) {
					try {
						conn.close();
					} catch (SQLException e) {
						e.printStackTrace();
						return null;
					}
				}
			}
		}

		//日付詳細データ取得メソッド(収入版)
		public List<Subject> dayDtIn(String date,int userId){

			Connection conn = null;
			List<Subject> dayList = new ArrayList<Subject>();
			try {
				// データベースへ接続
				conn = DBManager.getConnection();

				//  ここに処理を書いていく
				String sql = " select" +
						" household.id," +
						" household.user_id," +
						" subject.type_id," +
						" subject.subject_name," +
						" household.money," +
						" household.create_date," +
						"household.memo"+
						" from" +
						"  subject right outer join household" +
						"  on" +
						"  household.subject_id=subject.subject_id" +
						"  where" +
						"  subject.type_id=1" +
						"  and" +
						"  household.create_date=?"
						+ "  and"
						+ "  user_id=?;";

				PreparedStatement pStmt = conn.prepareStatement(sql);
				pStmt.setString(1, date);
				pStmt.setInt(2, userId);
				ResultSet rs = pStmt.executeQuery();

				while (rs.next()) {
				String subNameIn = rs.getString("subject_name");
				int subMoneyIn = rs.getInt("money");
				String suMemoIn= rs.getString("memo");
				Subject sub=new Subject(subNameIn,subMoneyIn,suMemoIn);
				dayList.add(sub);
				}
				return dayList;
			} catch (SQLException e) {
				e.printStackTrace();
				return null;
			} finally {
				// データベース切断
				if (conn != null) {
					try {
						conn.close();
					} catch (SQLException e) {
						e.printStackTrace();
						return null;
					}
				}
			}
		}


		//日付詳細データ取得メソッド(支出版)
				public List<Subject> dayDtEx(String date,int userId){

					Connection conn = null;
					List<Subject> dayList = new ArrayList<Subject>();
					try {
						// データベースへ接続
						conn = DBManager.getConnection();

						//  ここに処理を書いていく
						String sql = " select" +
								" household.id," +
								" household.user_id," +
								" subject.type_id," +
								" subject.subject_name," +
								" household.money," +
								" household.create_date," +
								"household.memo"+
								" from" +
								"  subject right outer join household" +
								"  on" +
								"  household.subject_id=subject.subject_id" +
								"  where" +
								"  subject.type_id=2" +
								"  and" +
								"  household.create_date=?"
								+ "  and"
								+ "  user_id=?;";

						PreparedStatement pStmt = conn.prepareStatement(sql);
						pStmt.setString(1, date);
						pStmt.setInt(2, userId);
						ResultSet rs = pStmt.executeQuery();

						while (rs.next()) {
						String subNameEx = rs.getString("subject_name");
						int subMoneyEx = rs.getInt("money");
						String suMemoEx= rs.getString("memo");
						Subject sub=new Subject(subNameEx,subMoneyEx,suMemoEx);
						dayList.add(sub);
						}
						return dayList;
					} catch (SQLException e) {
						e.printStackTrace();
						return null;
					} finally {
						// データベース切断
						if (conn != null) {
							try {
								conn.close();
							} catch (SQLException e) {
								e.printStackTrace();
								return null;
							}
						}
					}
				}

				//日付詳細データ取得メソッド(編集用)

				public Subject subEdit(int editId,int userId){

					Connection conn = null;
					try {
						// データベースへ接続
						conn = DBManager.getConnection();

						//  ここに処理を書いていく
						String sql = "select "+
							    "household.id,"+
								"household.user_id,"+
								"subject.type_id,"+
								"household.subject_id,"+
							    "subject.subject_name,"+
								"household.money,"+
								"household.create_date,"+
								"household.memo "+
								"from "+
								"subject right outer join household "+
								"on "+
								"household.subject_id=subject.subject_id "+
							    "where "+
								"household.id=? "+
								"and "+
								"user_id=?";

						PreparedStatement pStmt = conn.prepareStatement(sql);
						pStmt.setInt(1, editId);
						pStmt.setInt(2, userId);
						ResultSet rs = pStmt.executeQuery();

						int id=0;
						int subId=0;
						int typeId =0;
						int moneyEd=0;
						String memo=null;

						while (rs.next()) {
						id=rs.getInt("id");
						subId = rs.getInt("subject_id");
						typeId = rs.getInt("type_id");
						moneyEd = rs.getInt("money");
						memo= rs.getString("memo");
						}
						return new Subject(id,subId,typeId,moneyEd,memo);

					} catch (SQLException e) {
						e.printStackTrace();
						return null;
					} finally {
						// データベース切断
						if (conn != null) {
							try {
								conn.close();
							} catch (SQLException e) {
								e.printStackTrace();
								return null;
							}
						}
					}
				}


				//科目名取得メソッド(分析画面用)
				public ArrayList<String> subjectGet(int typeId) {

					Connection conn = null;
					ArrayList<String>subListIn=new ArrayList<String>();
					try {
						// データベースへ接続
						conn = DBManager.getConnection();

						//  ここに処理を書いていく
						String sql = "SELECT * FROM subject WHERE type_id = ?";

						PreparedStatement pStmt = conn.prepareStatement(sql);
						pStmt.setInt(1, typeId);
						ResultSet rs = pStmt.executeQuery();


						while (rs.next()) {
						String subjectName = rs.getString("subject_name");
						subListIn.add(subjectName);
						}
						return subListIn;
					} catch (SQLException e) {
						e.printStackTrace();
						return null;
					} finally {
						// データベース切断
						if (conn != null) {
							try {
								conn.close();
							} catch (SQLException e) {
								e.printStackTrace();
								return null;
							}
						}
					}
				}


				//科目名別金額取得メソッド
				public int moneyGet(String subjectName,int userIdAn,String createDate) {

					Connection conn = null;

					try {
						// データベースへ接続
						conn = DBManager.getConnection();

						//  ここに処理を書いていく
						String sql = "SELECT "+
								"household.id," +
								"household.user_id," +
								"household.subject_id," +
								"subject.subject_name," +
								"household.money," +
								"household.create_date," +
								"household.memo "+
								"FROM "+
								"subject right outer join household "+
								"on "+
								"household.subject_id=subject.subject_id "+
								"WHERE "+
								"subject.subject_name =? "+
								"AND "+
								"user_id=? "+
								"AND "+
								"household.create_date=?";

						PreparedStatement pStmt = conn.prepareStatement(sql);
						pStmt.setString(1, subjectName);
						pStmt.setInt(2, userIdAn);
						pStmt.setString(3, createDate);
						ResultSet rs = pStmt.executeQuery();

						int totalMoneyAn=0;
						while (rs.next()) {
						totalMoneyAn += rs.getInt("money");
						}
						return totalMoneyAn;
					} catch (SQLException e) {
						e.printStackTrace();
						return 0;
					} finally {
						// データベース切断
						if (conn != null) {
							try {
								conn.close();
							} catch (SQLException e) {
								e.printStackTrace();
								return 0;
							}
						}
					}
				}


				//合計金額取得メソッド
				public int total(int typeId,int userIdAn,String createDate) {

					Connection conn = null;

					try {
						// データベースへ接続
						conn = DBManager.getConnection();

						//  ここに処理を書いていく
						String sql = "SELECT "+
								"household.id, " +
								"household.user_id, " +
								"household.subject_id, " +
								"subject.type_id, "+
								"subject.subject_name, "+
								"household.create_date, "+
								"household.money " +
								"FROM "+
								"subject right outer join household "+
								"on "+
								"household.subject_id=subject.subject_id "+
								"WHERE "+
								"subject.type_id =? "+
								"AND "+
								"user_id=? "+
								"AND "+
								"household.create_date=?";


						PreparedStatement pStmt = conn.prepareStatement(sql);
						pStmt.setInt(1, typeId);
						pStmt.setInt(2, userIdAn);
						pStmt.setString(3, createDate);
						ResultSet rs = pStmt.executeQuery();

						int totalMoneyAn=0;
						while (rs.next()) {
						totalMoneyAn += rs.getInt("money");
						}
						return totalMoneyAn;
					} catch (SQLException e) {
						e.printStackTrace();
						return 0;
					} finally {
						// データベース切断
						if (conn != null) {
							try {
								conn.close();
							} catch (SQLException e) {
								e.printStackTrace();
								return 0;
							}
						}
					}
				}


				//科目更新メソッド
				public int subUpdate(String SubEdDate,int subEdNm,int subEdMoney,String subEdMemo,int subEdId) {

					Connection conn=null;

					try {
						conn=DBManager.getConnection();

						String sql="UPDATE household SET "
								+ "create_date=?,"
								+ "subject_id=?,"
								+ "money=?,"
								+ "memo=? "
								+ "WHERE id=?";

						PreparedStatement pStmt = conn.prepareStatement(sql);
						pStmt.setString(1, SubEdDate);
						pStmt.setInt(2, subEdNm);
						pStmt.setInt(3, subEdMoney);
						pStmt.setString(4, subEdMemo);
						pStmt.setInt(5, subEdId);
						int rs=pStmt.executeUpdate();
						return rs;
					} catch (SQLException e) {
						e.printStackTrace();
						return 0;
					} finally {
						// データベース切断
						if (conn != null) {
							try {
								conn.close();
							} catch (SQLException e) {
								e.printStackTrace();
								return 0;
							}
						}
					}
				}


				//科目削除メソッド
				public int delete(int deleteId) {

					Connection conn=null;

					try {
						conn=DBManager.getConnection();

						String sql="DELETE FROM household WHERE id=?";

						PreparedStatement pStmt = conn.prepareStatement(sql);
						pStmt.setInt(1, deleteId);

						int rs=pStmt.executeUpdate();
						return rs;
					} catch (SQLException e) {
						e.printStackTrace();
						return 0;
					} finally {
						// データベース切断
						if (conn != null) {
							try {
								conn.close();
							} catch (SQLException e) {
								e.printStackTrace();
								return 0;
							}
						}
					}
				}


				//科目削除画面表示メソッド

				public Subject subDelete(int editId,int userId){

					Connection conn = null;
					try {
						// データベースへ接続
						conn = DBManager.getConnection();

						//  ここに処理を書いていく
						String sql = "select "+
							    "household.id,"+
								"household.user_id,"+
								"subject.type_id,"+
								"household.subject_id,"+
							    "subject.subject_name,"+
								"household.money,"+
								"household.create_date,"+
								"household.memo "+
								"from "+
								"subject right outer join household "+
								"on "+
								"household.subject_id=subject.subject_id "+
							    "where "+
								"household.id=? "+
								"and "+
								"user_id=?";

						PreparedStatement pStmt = conn.prepareStatement(sql);
						pStmt.setInt(1, editId);
						pStmt.setInt(2, userId);
						ResultSet rs = pStmt.executeQuery();

						int id=0;
						String deleteDate=null;
						String deleteSubName=null;
						int moneyDe=0;
						String memo=null;

						while (rs.next()) {
						id=rs.getInt("id");
						deleteDate = rs.getString("create_date");
						deleteSubName = rs.getString("subject_name");
						moneyDe = rs.getInt("money");
						memo= rs.getString("memo");
						}
						return new Subject(id,deleteDate,deleteSubName,moneyDe,memo);

					} catch (SQLException e) {
						e.printStackTrace();
						return null;
					} finally {
						// データベース切断
						if (conn != null) {
							try {
								conn.close();
							} catch (SQLException e) {
								e.printStackTrace();
								return null;
							}
						}
					}
				}








}
