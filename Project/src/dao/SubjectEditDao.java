package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class SubjectEditDao {


	//科目削除メソッド
	public int delete(int subjectId) {

		Connection conn=null;

		try {
			conn=DBManager.getConnection();

			String sql="DELETE FROM subject where subject_id=?;";

			PreparedStatement pStmt = conn.prepareStatement(sql);

			pStmt.setInt(1, subjectId);
			int rs=pStmt.executeUpdate();
			return rs;
		} catch (SQLException e) {
			e.printStackTrace();
			return 0;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return 0;
				}
			}
		}
	}


	//新規科目登録メソッド
	public int insert(int typeId,String subjectName) {

		Connection conn=null;

		try {
			conn=DBManager.getConnection();

			String sql="INSERT INTO subject(type_id,subject_name) VALUES(?,?);";

			PreparedStatement pStmt = conn.prepareStatement(sql);

			pStmt.setInt(1, typeId);
			pStmt.setString(2, subjectName);
			int rs=pStmt.executeUpdate();
			return rs;
		} catch (SQLException e) {
			e.printStackTrace();
			return 0;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return 0;
				}
			}
		}
	}

	}


