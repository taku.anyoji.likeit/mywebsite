package dao;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.xml.bind.DatatypeConverter;

import model.UserBeans;

public class UserDao {

	 //暗号化メソッド

	public String pass(String password){
		//ハッシュを生成したい元の文字列
				String source = password;
				//ハッシュ生成前にバイト配列に置き換える際のCharset
				Charset charset = StandardCharsets.UTF_8;
				//ハッシュアルゴリズム
				String algorithm = "MD5";

				//ハッシュ生成処理
				byte[] bytes = null;
				try {
					bytes = MessageDigest.getInstance(algorithm).digest(source.getBytes(charset));
				} catch (NoSuchAlgorithmException e) {
					e.printStackTrace();
				}
				String result = DatatypeConverter.printHexBinary(bytes);

				return result;
	}



	//ログインメソッド
	public UserBeans findByLoginInfo(String loginId, String MDPass) {

		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			//  ここに処理を書いていく
			String sql = "SELECT * FROM user WHERE login_id =? and password = ?";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, loginId);
			pStmt.setString(2, MDPass);
			ResultSet rs = pStmt.executeQuery();

			// ログイン失敗時の処理		・・・①
			if (false == rs.next()) {
				return null;
			}
			// ログイン成功時の処理		・・・②
			int userIdDate = rs.getInt("user_id");
			String loginIdDate = rs.getString("login_id");
			String nameDate = rs.getString("user_name");
			String birthDate = rs.getString("birth_date");
			return new UserBeans(userIdDate,loginIdDate, nameDate, birthDate);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	//新規登録メソッド
	public int insert(String registLoId,String MDPass,String registUsNm,String registBrDt) {

		Connection conn=null;

		try {
			conn=DBManager.getConnection();

			String sql="INSERT INTO user(login_id,user_name,password,birth_date) "
					+ "VALUES(?,?,?,?);";
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, registLoId);
			pStmt.setString(2, registUsNm);
			pStmt.setString(3, MDPass);
			pStmt.setString(4, registBrDt);

			int rs=pStmt.executeUpdate();
			return rs;
		} catch (SQLException e) {
			e.printStackTrace();
			return 0;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return 0;
				}
			}
		}
	}

}
