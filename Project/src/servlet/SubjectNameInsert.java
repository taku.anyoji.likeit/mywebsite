package servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.SubjectEditDao;
import model.ParseInt;
import model.UserBeans;

/**
 * Servlet implementation class SubjectNameInsert
 */
@WebServlet("/SubjectNameInsert")
public class SubjectNameInsert extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public SubjectNameInsert() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("UTF-8");

		HttpSession session = request.getSession();
		UserBeans u = (UserBeans)session.getAttribute("userInfo");

		if(u==null) {
			response.sendRedirect("Login");
			return;
		}else {


			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/SubjectNameInsert.jsp");
			dispatcher.forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("UTF-8");

		int subNmInId=ParseInt.parseInt(request.getParameter("SubjectInType"));
		String SubjectNameIn=request.getParameter("SubjectNameIn");

		//登録失敗パターン
		if(SubjectNameIn.length()<1) {
			request.setAttribute("error", "科目名称が未入力です");
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/SubjectNameInsert.jsp");
			dispatcher.forward(request, response);
			return;
		}

		SubjectEditDao subEdDao=new SubjectEditDao();
		int subNmInRs=subEdDao.insert(subNmInId,SubjectNameIn);

		if(subNmInRs==0) {
			response.sendRedirect("SubjectInsert");
			return;
		}else {
			response.sendRedirect("SubjectInsert");
			return;
		}


	}

}
