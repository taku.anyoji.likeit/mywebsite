package servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.SubjectDao;
import model.ParseInt;
import model.Subject;
import model.UserBeans;

/**
 * Servlet implementation class ExpenseRegist
 */
@WebServlet("/ExpenseRegist")
public class ExpenseRegist extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public ExpenseRegist() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("UTF-8");


		HttpSession session = request.getSession();
		UserBeans u = (UserBeans)session.getAttribute("userInfo");

		if(u==null) {
			response.sendRedirect("Login");
			return;
		}else {
			int expenseSub=2;
			SubjectDao subDao=new SubjectDao();
			List<Subject> subList=subDao.subject(expenseSub);
			request.setAttribute("subList", subList);
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/ExpenseRegist.jsp");
		dispatcher.forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("UTF-8");

		int exUsId=ParseInt.parseInt(request.getParameter("ExpenseUserId"));
		String exDate=request.getParameter("ExpenseDate");
		String exMemo=request.getParameter("ExpenseMemo");

		int exRegist=0;
		int exMoney=0;
		try {
			exRegist=Integer.parseInt(request.getParameter("ExpenseRegist"));
			exMoney=Integer.parseInt(request.getParameter("ExpenseMoney"));
		}catch( NumberFormatException e ) {
			int expenseSub=2;
			SubjectDao subDao=new SubjectDao();
			List<Subject> subList=subDao.subject(expenseSub);
			request.setAttribute("subList", subList);
			 request.setAttribute("error", "入力された内容は正しくありません");
			 RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/ExpenseRegist.jsp");
				dispatcher.forward(request, response);
				return;
		}


		SubjectDao suDao=new SubjectDao();

		//登録失敗パターン
		if(exDate.length()<1||exRegist==0) {
			int expenseSub=2;
			SubjectDao subDao=new SubjectDao();
			List<Subject> subList=subDao.subject(expenseSub);
			request.setAttribute("subList", subList);
			request.setAttribute("error", "入力された内容は正しくありません");
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/ExpenseRegist.jsp");
			dispatcher.forward(request, response);
			return;
		}

		//登録成功パターン
		int result=suDao.insert(exUsId, exDate, exRegist, exMoney, exMemo);
		if(result!=0) {
			response.sendRedirect("HomeMonth");
			return;
		}else {
			int expenseSub=2;
			SubjectDao subDao=new SubjectDao();
			List<Subject> subList=subDao.subject(expenseSub);
			request.setAttribute("subList", subList);
			request.setAttribute("error", "入力された内容は正しくありません");
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/ExpenseRegist.jsp");
			dispatcher.forward(request, response);
			return;
		}
		}
	}


