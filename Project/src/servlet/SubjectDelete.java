package servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.SubjectDao;
import model.ParseInt;
import model.Subject;
import model.UserBeans;

/**
 * Servlet implementation class SubjectDelete
 */
@WebServlet("/SubjectDelete")
public class SubjectDelete extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public SubjectDelete() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("UTF-8");

		HttpSession session = request.getSession();
		UserBeans u = (UserBeans)session.getAttribute("userInfo");

		if(u==null) {
			response.sendRedirect("Login");
		}else {


			int year = ParseInt.parseInt(request.getParameter("year"));
			int month = ParseInt.parseInt(request.getParameter("month"));
			int date = ParseInt.parseInt(request.getParameter("date"));
			int deleteId= ParseInt.parseInt(request.getParameter("id"));
			int userId=u.getUserId();

			if(year==0||month==0||date==0||deleteId==0) {
				response.sendRedirect("DayDetail");
				return;
			}

			SubjectDao subDao=new SubjectDao();
		    Subject subDelete=subDao.subDelete(deleteId,userId);

		    request.setAttribute("subDelete", subDelete);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/SubjectDelete.jsp");
		dispatcher.forward(request, response);
		return;
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("UTF-8");

		int deleteId= ParseInt.parseInt(request.getParameter("deleteId"));

		SubjectDao subDao=new SubjectDao();
		int deleteResult=subDao.delete(deleteId);

		if(deleteResult!=0) {
			response.sendRedirect("HomeMonth");
			return;
		}else {
			response.sendRedirect("HomeMonth");
			return;
		}
	}

}
