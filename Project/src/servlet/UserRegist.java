package servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;
import model.UserBeans;

/**
 * Servlet implementation class UserRegist
 */
@WebServlet("/UserRegist")
public class UserRegist extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserRegist() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("UTF-8");

		HttpSession session = request.getSession();
		UserBeans u = (UserBeans)session.getAttribute("userInfo");

		if(u!=null) {
			response.sendRedirect("HomeMonth");
			return;
		}else {
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/UserRegist.jsp");
		dispatcher.forward(request, response);
		return;
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("UTF-8");

		String registLoId=request.getParameter("registLoId");
		String registPass=request.getParameter("registPass");
		String registPs=request.getParameter("registPs");
		String registUsNm=request.getParameter("registUsNm");
		String registBrDt=request.getParameter("registBrDt");

		UserDao userDao=new UserDao();

		//登録失敗パターン
		if(registLoId.length()<1||registUsNm.length()<1||registBrDt.length()<1) {
			request.setAttribute("error", "入力された内容は正しくありません");
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/UserRegist.jsp");
			dispatcher.forward(request, response);
			return;
		}
	if(!(registPass.equals( registPs))) {
		request.setAttribute("error", "入力された内容は正しくありません");
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/UserRegist.jsp");
		dispatcher.forward(request, response);
		return;
	}

	//登録成功パターン
	String MDPass=userDao.pass(registPass);
	int result=userDao.insert(registLoId, MDPass, registUsNm, registBrDt);
	if(result!=0) {
		response.sendRedirect("Login");
		return;
	}else {
		request.setAttribute("error", "入力された内容は正しくありません");
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/UserRegist.jsp");
		dispatcher.forward(request, response);
		return;
	}
	}

}
