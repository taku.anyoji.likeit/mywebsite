package servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.SubjectDao;
import model.ParseInt;
import model.Subject;
import model.UserBeans;

/**
 * Servlet implementation class DayDetail
 */
@WebServlet("/DayDetail")
public class DayDetail extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public DayDetail() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("UTF-8");

		HttpSession session = request.getSession();
		UserBeans u = (UserBeans)session.getAttribute("userInfo");

		if(u==null) {
			response.sendRedirect("Login");
			return;
		}else {
			int userId=u.getUserId();

			int year =ParseInt.parseInt(request.getParameter("year"));
			int month =ParseInt.parseInt(request.getParameter("month"));
			int date =ParseInt.parseInt(request.getParameter("date"));

			if(year==0||month==0||date==0) {
				response.sendRedirect("HomeMonth");
				return;
			}

		SubjectDao subDao=new SubjectDao();
		String DayDt= String.format("%04d", year)+String.format("%02d",  month)+String.format("%02d", date);
		List<Subject> subList=subDao.dayDetail(DayDt,userId);

		request.setAttribute("thisYear", year);
		request.setAttribute("thisMonth", month);
		request.setAttribute("thisDate", date);
		request.setAttribute("subList", subList);


		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/DayDetail.jsp");
		dispatcher.forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("UTF-8");

		response.sendRedirect("HomeMonth");
	}

}
