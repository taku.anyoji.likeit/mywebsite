package servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.SubjectDao;
import model.ParseInt;
import model.Subject;
import model.UserBeans;

/**
 * Servlet implementation class SubjectEdit
 */
@WebServlet("/SubjectEdit")
public class SubjectEdit extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public SubjectEdit() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("UTF-8");

		HttpSession session = request.getSession();
		UserBeans u = (UserBeans)session.getAttribute("userInfo");

		if(u==null) {
			response.sendRedirect("Login");
		}else {


			int year = ParseInt.parseInt(request.getParameter("year"));
			int month = ParseInt.parseInt(request.getParameter("month"));
			int date = ParseInt.parseInt(request.getParameter("date"));
			int editId= ParseInt.parseInt(request.getParameter("id"));
			int userId=u.getUserId();

			if(year==0||month==0||date==0||editId==0) {
				response.sendRedirect("DayDetail");
				return;
			}

			SubjectDao subDao=new SubjectDao();

		    Subject subEd=subDao.subEdit(editId,userId);

		    int typeIdEd=subEd.getTypeId();
		    SubjectDao subDaoSe=new SubjectDao();
			List<Subject> subList=subDaoSe.subject(typeIdEd);

			session.setAttribute("editId", editId);
			session.setAttribute("thisYear", year);
			session.setAttribute("thisMonth", month);
			session.setAttribute("thisDate", date);
			session.setAttribute("subEd",subEd);
			session.setAttribute("subList",subList);

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/SubjectEdit.jsp");
			dispatcher.forward(request, response);
	}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("UTF-8");

		int subEditId=ParseInt.parseInt(request.getParameter("subEditId"));
		String SubEdDate=request.getParameter("SubEdDate");
		int subEdUserId=ParseInt.parseInt(request.getParameter("SubEdUserId"));
		int subEdNm=ParseInt.parseInt(request.getParameter("SubjectEdName"));
		int subEdMoney=ParseInt.parseInt(request.getParameter("money"));
		String subEdMemo=request.getParameter("memo");


		SubjectDao subDao=new SubjectDao();
		int subEdResult=subDao.subUpdate(SubEdDate,subEdNm,subEdMoney,subEdMemo,subEditId);

		if(subEdResult==0) {
			request.setAttribute("error", "入力された内容は正しくありません");
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/SubjectEdit.jsp");
			dispatcher.forward(request, response);
			return;
		}else {
			response.sendRedirect("HomeMonth");
			return;

		}
	}

}
