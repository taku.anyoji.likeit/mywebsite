package servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.SubjectDao;
import model.CalemdarData;
import model.ParseInt;
import model.Subject;
import model.UserBeans;
import model.WeekTotal;

/**
 * Servlet implementation class HomeMonth
 */
@WebServlet("/HomeMonth")
public class HomeMonth extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public HomeMonth() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("UTF-8");

		HttpSession session = request.getSession();
		UserBeans u = (UserBeans)session.getAttribute("userInfo");

		if(u==null) {
			response.sendRedirect("Login");
			return;
		}else {
			Calendar calendar = Calendar.getInstance();

			int userId=u.getUserId();
			int year=0;
			int month=0;

			String provisionalYear=request.getParameter("year");
			String provisionalNextMonth=request.getParameter("NextMonth");
			String provisionalPreviousMonth=request.getParameter("PreviousMonth");

			if(provisionalYear==null) {
				year = calendar.get(Calendar.YEAR);
				month = calendar.get(Calendar.MONTH)+1;
			}
			else if(provisionalYear!=null && provisionalNextMonth!=null) {
				year=ParseInt.parseInt(provisionalYear);
				month=ParseInt.parseInt(provisionalNextMonth);
			}
			else if(provisionalYear!=null && provisionalPreviousMonth!=null) {
				year=ParseInt.parseInt(provisionalYear);
				month=ParseInt.parseInt(provisionalPreviousMonth);
			}

			if(month==13) {
				month=1;
				year+=1;
			}
			else if(month==0) {
				month=12;
				year-=1;
			}


			    int day = calendar.get(Calendar.DATE);
			    int nextMonth=month+1;
			    int previousMonth=month-1;

			    request.setAttribute("previousMonth",previousMonth);
			    request.setAttribute("nextMonth",nextMonth);
			    request.setAttribute("thisYear",year);
			    request.setAttribute("thisMonth",month);

			    System.out.print("本日の日時は");
			    System.out.println(year + "年" + (month) + "月" + day + "日");

			    /* 今月が何曜日から開始されているか確認する */
			    calendar.set(year, month-1, 1);
			    int startWeek = calendar.get(Calendar.DAY_OF_WEEK);
			    System.out.println("今月の曜日は" + startWeek+ "から");


			    /* 先月が何日までだったかを確認する */
			    calendar.set(year, month-1, 0);
			    int beforeMonthlastDay = calendar.get(Calendar.DATE);
			    System.out.println("先月は" + beforeMonthlastDay + "日まで");

			    /* 今月が何日までかを確認する */
			    calendar.set(year, month, 0);
			    int thisMonthlastDay = calendar.get(Calendar.DATE);
			    System.out.println("今月は" + thisMonthlastDay + "日まで");

			    List<CalemdarData> calemdarDataList = new ArrayList<CalemdarData>(42);
			    int count = 0;

			    /* 先月分の日付を格納する */
			    for (int i = startWeek - 2 ; i >= 0 ; i--){
			    	CalemdarData calemdarData = new CalemdarData();
			    	calemdarData.setDate(0);
			    	calemdarData.setTotalEx(0);
			    	calemdarData.setTotalIn(0);
			      calemdarDataList.add(calemdarData);
			      count++;
			    }

			    /* 今月分の日付を格納する */
			    for (int i = 1 ; i <= thisMonthlastDay ; i++){
			    	CalemdarData calemdarData = new CalemdarData();
			    	calemdarData.setDate(i);
			    	count++;
			    	SubjectDao subDao=new SubjectDao();
				    String date= String.format("%04d", year)+String.format("%02d",  month)+String.format("%02d", i);
					List<Subject> subList=subDao.dayDtEx(date,userId);
					if(subList==null) {
						calemdarData.setTotalEx(0);
					}else {
					int total = 0;
					for (Subject sub : subList) {
						total += sub.getMoney();
					}
					calemdarData.setTotalEx(total);
					}

					List<Subject> subListB=subDao.dayDtIn(date,userId);
					if(subListB==null) {
						calemdarData.setTotalEx(0);
					}else {
					int total = 0;
					for (Subject subB : subListB) {
						total += subB.getMoney();
					}
					calemdarData.setTotalIn(total);
					}
			    	calemdarDataList.add(calemdarData);
			    }

			    /* 翌月分の日付を格納する */
			    int nextMonthDay = 1;
			    while (count % 7 != 0){
			    	CalemdarData calemdarData = new CalemdarData();
			    	calemdarData.setDate(0);
			    	calemdarData.setTotalEx(0);
			    	calemdarData.setTotalIn(0);
			      count++;
			      calemdarDataList.add(calemdarData);
			    }
			   System.out.println(count);

			    int[] weekCount = new int[(count / 7)];
			    for(int y=0; y<weekCount.length; y++) {
			    	weekCount[y]=y;
			    }


			    List<WeekTotal> weekTotalList = new ArrayList<WeekTotal>();
			    for (int i = 0 ; i < weekCount.length ; i++){
			    	CalemdarData calemdarDataT = new CalemdarData();
			    	WeekTotal weekTotal=new WeekTotal();
			    	int totalIn=0;
			    	int totalEx=0;
				      for (int j = i * 7 ;   j < i * 7 + 7 ;   j++){
				    	  calemdarDataT= calemdarDataList.get(j);
				    	  totalIn+=calemdarDataT.getTotalIn();
				    	  totalEx+=calemdarDataT.getTotalEx();
				      }
				      weekTotal.setWeekTotalIn(totalIn);
				      weekTotal.setWeekTotalEx(totalEx);
				      weekTotalList.add(weekTotal);
				    }



			    request.setAttribute("weekTotalList", weekTotalList);
			    request.setAttribute("weekCount", weekCount);
			    request.setAttribute("calemdarDataList", calemdarDataList);
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/HomeMonth.jsp");
		dispatcher.forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
