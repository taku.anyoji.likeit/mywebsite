package servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.SubjectEditDao;
import model.ParseInt;
import model.UserBeans;

/**
 * Servlet implementation class SubjectNameDelete
 */
@WebServlet("/SubjectNameDelete")
public class SubjectNameDelete extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public SubjectNameDelete() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("UTF-8");

		HttpSession session = request.getSession();
		UserBeans u = (UserBeans)session.getAttribute("userInfo");

		if(u==null) {
			response.sendRedirect("Login");
			return;
		}else {

			int subjectId=ParseInt.parseInt(request.getParameter("subjectId"));
			SubjectEditDao subDao=new SubjectEditDao();
			int deleteRs=subDao.delete(subjectId);

			if(deleteRs==0) {
				response.sendRedirect("SubjectInsert");
				return;
			}else {
				response.sendRedirect("SubjectInsert");
				return;
			}

		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

	}

}
