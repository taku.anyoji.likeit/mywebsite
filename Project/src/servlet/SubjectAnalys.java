package servlet;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.google.gson.Gson;

import dao.SubjectDao;
import model.ParseInt;
import model.UserBeans;

/**
 * Servlet implementation class SubjectAnalys
 */
@WebServlet("/SubjectAnalys")
public class SubjectAnalys extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public SubjectAnalys() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("UTF-8");

		HttpSession session = request.getSession();
		UserBeans u = (UserBeans)session.getAttribute("userInfo");


		if(u==null) {
			response.sendRedirect("Login");
			return;
		}else {
			int userIdAn=u.getUserId();
			int year=ParseInt.parseInt(request.getParameter("year"));
			int month=ParseInt.parseInt(request.getParameter("month"));

			int date[]=new int[31];
			for(int i=0; i<date.length; i++) {
				date[i]=i+1;
			}

			String ymd[]=new String[31] ;
			for(int i=0; i<ymd.length; i++) {
				 ymd[i]=String.format("%04d", year)+String.format("%02d",  month)+String.format("%02d",date[i]);
			}



			Gson gson = new Gson();

			//科目名取得→配列に挿入
			//収入
			int typeIn=1;
			SubjectDao subDao=new SubjectDao();
			ArrayList<String>subListIn=subDao.subjectGet(typeIn);
			request.setAttribute("subListIn", gson.toJson(subListIn));

			//支出
			int typeEx=2;
			SubjectDao subDaoEx=new SubjectDao();
			ArrayList<String>subListEx=subDaoEx.subjectGet(typeEx);
			request.setAttribute("subListEx", gson.toJson(subListEx));

			//それぞれの値の取得
			//収入
			SubjectDao subDaoInNm=new SubjectDao();
			ArrayList<Integer> subListInNm = new ArrayList<Integer>();
			for(int i=0; i<subListIn.size(); i++) {
				int income=0;
				for(int j=0; j<ymd.length; j++) {
					income+=subDaoInNm.moneyGet(subListIn.get(i),userIdAn,ymd[j]);
				}
				subListInNm.add(income);
			}
			request.setAttribute("subListInNm", gson.toJson(subListInNm));

			//支出
			SubjectDao subDaoExNm=new SubjectDao();
			ArrayList<Integer> subListExNm = new ArrayList<Integer>();
			for(int i=0; i<subListEx.size(); i++) {
				int expense=0;
				for(int j=0; j<ymd.length; j++) {
					expense+=subDaoExNm.moneyGet(subListEx.get(i),userIdAn,ymd[j]);
				}
				subListExNm.add(expense);
			}
			request.setAttribute("subListExNm", gson.toJson(subListExNm));


			//合計値の計算
			//収入
			int typeIdIn=1;
			int totalIn=0;
			SubjectDao subDaoTotalIn=new SubjectDao();
			for(int i=0; i<ymd.length; i++ ) {
				totalIn+=subDaoTotalIn.total(typeIdIn,userIdAn,ymd[i]);
			}
			request.setAttribute("totalIn", gson.toJson(totalIn));

			//支出
			int typeIdEx=2;
			int totalEx=0;
			SubjectDao subDaoTotalEx=new SubjectDao();
			for(int i=0; i<ymd.length; i++ ) {
			totalEx+=subDaoTotalEx.total(typeIdEx,userIdAn,ymd[i]);
			}
			request.setAttribute("totalEx", gson.toJson(totalEx));

			request.setAttribute("month", month);



		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/SubjectAnalys.jsp");
		dispatcher.forward(request, response);
		return;
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

	}

}
