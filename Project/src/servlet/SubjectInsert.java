package servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.SubjectDao;
import model.Subject;
import model.UserBeans;

/**
 * Servlet implementation class SubjectInsert
 */
@WebServlet("/SubjectInsert")
public class SubjectInsert extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public SubjectInsert() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("UTF-8");

		HttpSession session = request.getSession();
		UserBeans u = (UserBeans)session.getAttribute("userInfo");

		if(u==null) {
			response.sendRedirect("Login");
			return;
		}else {

			SubjectDao subDao=new SubjectDao();

			//科目名取得
			//収入
			int subIn=1;
			List<Subject> subListIn=subDao.subject(subIn);
			int listIns=subListIn.size();
			request.setAttribute("listIns", listIns);
			request.setAttribute("subListIn", subListIn);


			//支出
			int subEx=2;
			List<Subject> subListEx=subDao.subject(subEx);
			int listExs=subListEx.size();
			request.setAttribute("listExs", listExs);
			request.setAttribute("subListEx", subListEx);

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/SubjectInsert.jsp");
			dispatcher.forward(request, response);

		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

	}

}
