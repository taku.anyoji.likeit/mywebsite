package servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.SubjectDao;
import model.Subject;
import model.UserBeans;

/**
 * Servlet implementation class IncomeRegist
 */
@WebServlet("/IncomeRegist")
public class IncomeRegist extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public IncomeRegist() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("UTF-8");

		HttpSession session = request.getSession();
		UserBeans u = (UserBeans)session.getAttribute("userInfo");

		if(u==null) {
			response.sendRedirect("Login");
			return;
		}else {
			int incomeSub=1;
			SubjectDao subDao=new SubjectDao();
			List<Subject> subList=subDao.subject(incomeSub);
			request.setAttribute("subList", subList);
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/IncomeRegist.jsp");
		dispatcher.forward(request, response);
		return;
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("UTF-8");

		int inUsId=Integer.parseInt(request.getParameter("IncomeUserId"));
		String inDate=request.getParameter("IncomeDate");
		String inMemo=request.getParameter("IncomeMemo");

		int inRegist=0;
		int inMoney=0;
		try {
			inRegist=Integer.parseInt(request.getParameter("IncomeRegist"));
			inMoney=Integer.parseInt(request.getParameter("IncomeMoney"));
		}catch( NumberFormatException e ) {
			int incomeSub=1;
			SubjectDao subDao=new SubjectDao();
			List<Subject> subList=subDao.subject(incomeSub);
			request.setAttribute("subList", subList);
			 request.setAttribute("error", "入力された内容は正しくありません");
			 RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/IncomeRegist.jsp");
				dispatcher.forward(request, response);
				return;
		}


		SubjectDao suDao=new SubjectDao();

		//登録失敗パターン
		if(inDate.length()<1||inRegist==0) {
			int incomeSub=1;
			SubjectDao subDao=new SubjectDao();
			List<Subject> subList=subDao.subject(incomeSub);
			request.setAttribute("subList", subList);
			request.setAttribute("error", "入力された内容は正しくありません");
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/IncomeRegist.jsp");
			dispatcher.forward(request, response);
			return;
		}

		//登録成功パターン
		int result=suDao.insert(inUsId, inDate, inRegist, inMoney, inMemo);
		if(result!=0) {
			response.sendRedirect("HomeMonth");
			return;
		}else {
			int incomeSub=1;
			SubjectDao subDao=new SubjectDao();
			List<Subject> subList=subDao.subject(incomeSub);
			request.setAttribute("subList", subList);
			request.setAttribute("error", "入力された内容は正しくありません");
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/IncomeRegist.jsp");
			dispatcher.forward(request, response);
			return;
		}
		}
	}


