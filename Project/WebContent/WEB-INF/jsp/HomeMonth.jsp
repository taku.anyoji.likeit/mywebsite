<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ホーム画面</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<link href="css/HomeMonth.css" rel="stylesheet" type="text/css" />
</head>
<body>
<div class="head" align="right">
				<header>
			<div class="alert alert-dark" role="alert">
				${sessionScope.userInfo.userName}さん
					<a href="Logout" class="alert-link">ログアウト</a>
					<div class="row">
<div style = "margin-bottom:20px"></div>
</div>
			</div>
		</header>
	</div>
<div class="row">
<div style = "margin-bottom:45px"></div>
</div>
<c:choose>
<c:when test="${sessionScope.userInfo.userId ==1}">
<div class="alert alert-green" align="right" >
<a href="SubjectInsert" class="alert-link">科目編集</a>
</div>
</c:when>
<c:otherwise>
</c:otherwise>
</c:choose>
<div class="row">
<div style = "margin-bottom:45px"></div>
</div>

<div  align="center">
<div class="row title-area">
 <a href="HomeMonth?year=${thisYear}&PreviousMonth=${previousMonth}" class="arrow2 sample5-3"></a>
 <b>${thisYear}年 ${thisMonth}月カレンダー</b>
 <a href="HomeMonth?year=${thisYear}&NextMonth=${nextMonth}" class="arrow sample5-2"></a>
</div>

<div class="row">
<div style = "margin-bottom:50px"></div>
</div>
<table border='1'>
<tr>
<th  bgcolor='#fa8072'>
<strong>日曜日</strong>
</th>
<th  bgcolor='#ffebcd'>
<strong>月曜日</strong>
</th>
<th  bgcolor='#ffebcd'>
<strong>火曜日</strong>
</th>
<th  bgcolor='#ffebcd'>
<strong>水曜日</strong>
</th>
<th bgcolor='#ffebcd'>
<strong>木曜日</strong>
</th>
<th bgcolor='#ffebcd'>
<strong>金曜日</strong>
</th>
<th  bgcolor='#00bfff'>
<strong>土曜日</strong>
</th>
<th  bgcolor='#ffebcd'>
<strong>合計</strong>
</th>

</tr>
<c:forEach var="i" items="${weekCount}"  >
<tr>
<c:forEach var="j" items="${calemdarDataList}" begin="${i*7}" end="${(i*7)+6}" step="1" >
<td class="size" bgcolor='#fffafa'>
<c:choose>
<c:when test="${j.date==0}">
</c:when>
<c:otherwise>
<a href="DayDetail?year=${thisYear}&month=${thisMonth}&date=${j.date}" class="alert-link">${j.date}</a><br>
<br>
収入：<span class="income">${j.totalIn}</span><br>
支出：<span class="expense">${j.totalEx}</span>
</c:otherwise>
</c:choose>
</td>
</c:forEach>

<td>
収入合計：${weekTotalList[i].weekTotalIn}<br>
支出合計：${weekTotalList[i].weekTotalEx}
</td>

</tr>
</c:forEach>
</table>
</div>
<div class="row">
<div style = "margin-bottom:90px"></div>
</div>

<div class="row">
<div align="right" class="col-sm-5">
<a class="btn btn-warning" href="IncomeRegist">収入登録</a>　　　　　　　　　　
</div>
<div align="right" class="col-sm-1">
<a class="btn btn-success" href="ExpenseRegist">支出登録</a>　　　　　　　　　　
</div>
<div align="center" class="col-sm-4">
								<a class="btn btn-primary" href="SubjectAnalys?year=${thisYear}&month=${thisMonth}">
								分析
								</a>
								</div>
								</div>
</body>
</html>