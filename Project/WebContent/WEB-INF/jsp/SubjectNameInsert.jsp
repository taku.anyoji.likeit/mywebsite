<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
     <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>新科目登録</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
	<link href="css/SubjectNameInsert.css" rel="stylesheet" type="text/css" />
</head>
<body>
<div class="head" align="right">
		<header>
			<div class="alert alert-dark" role="alert">
				${sessionScope.userInfo.userName}さん
					<a href="Logout" class="alert-link">ログアウト</a>
					<div class="row">
<div style = "margin-bottom:20px"></div>
</div>
			</div>
		</header>
	</div>
	<div class="row">
<div style = "margin-bottom:90px"></div>
</div>
<div  align="center">
<p>新科目追加登録</p>
<div class="row">
<div style = "margin-bottom:25px"></div>
</div>
<div>
<font color="red">
<c:if test="${error != null}" >
	    <div class="alert alert-danger" role="alert">
		  ${error}
		</div>
	</c:if>
	</font>
	</div>
<div class="row">
<div style = "margin-bottom:25px"></div>
</div>
</div>
<form action="SubjectNameInsert" method="post">

<div class="row">
<div  class="col-sm-6" align="right"><h3>収支カテゴリ</h3></div>
<div  class="col-sm-2">
<h3>
<select  name="SubjectInType">
<option value="1">収入</option>
<option value="2">支出</option>
</select>
</h3>
</div>
</div>
<div style = "margin-bottom:50px"></div>
<div class="row">
<div  class="col-sm-6" align="right"><h3>科目名称</h3></div>
<div  class="col-sm-2">
<h3>
<input type="text" name="SubjectNameIn">
</h3>
</div>
</div>
<div style = "margin-bottom:50px"></div>
<div  class="text-center" >
<span class="mgr-10"><button type="button" onClick="history.back()">戻る</button></span>
<input type="submit" value="登録" >
</div>
</form>
</body>
</html>