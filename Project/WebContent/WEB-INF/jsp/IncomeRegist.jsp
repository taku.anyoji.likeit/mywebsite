<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>収入登録</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
	<link href="css/IncomeRegist.css" rel="stylesheet" type="text/css"/>
</head>
<body>
<div class="head" align="right">
		<header>
			<div class="alert alert-dark" role="alert">
				${sessionScope.userInfo.userName}さん
					<a href="Logout" class="alert-link">ログアウト</a>
					<div class="row">
<div style = "margin-bottom:20px"></div>
</div>
			</div>
		</header>
	</div>
	<div class="row">
<div style = "margin-bottom:90px"></div>
</div>
<div  align="center">
<b>収入登録</b>
<div class="row">
<div style = "margin-bottom:25px"></div>
</div>
<div>
<font color="red">
<c:if test="${error != null}" >
	    <div class="alert alert-danger" role="alert">
		  ${error}
		</div>
	</c:if>
	</font>
	</div>
	<div class="row">
<div style = "margin-bottom:25px"></div>
</div>
<form action="IncomeRegist" method="post">
<table>
<tr>
<td>日付</td>
<td></td>
<td><input type="date" name="IncomeDate">
<input type="hidden" name="IncomeUserId" value="${sessionScope.userInfo.userId}">
</td>
</tr>
<tr>
<td>科目</td>
<td></td>
<td><select  name="IncomeRegist">
<c:forEach var="sub" items="${subList}">
<option value="${sub.subjectId}">${sub.subjectName}</option>
</c:forEach>
</select>
</td>
</tr>
<tr>
<td>金額</td>
<td></td>
<td><input type="number" name="IncomeMoney" pattern=”0|[1-9][0-9]*” ></td>
</tr>
<tr>
<td>メモ</td>
<td></td>
<td><textarea name="IncomeMemo" cols="22"></textarea></td>
</tr>
</table>
<div class="row">
<div style = "margin-bottom:30px"></div>
</div>
<div class="row" >
 <div class="col-sm-3">
<a href="HomeMonth" class="alert-link">戻る</a>
</div>
<div  class="col-sm-6">​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​
			<input type="submit" value="登録">
			</div>
			</div>
			</form>
</div>

</body>
</html>