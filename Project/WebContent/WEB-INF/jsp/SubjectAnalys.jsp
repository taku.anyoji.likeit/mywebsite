<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
     <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8">
<title>収支分析画面</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
	<link href="css/SubjectAnalys.css" rel="stylesheet" type="text/css" />
</head>
<body>
<div class="head" align="right">
		<header>
			<div class="alert alert-dark" role="alert">
				${sessionScope.userInfo.userName}さん
					<a href="Logout" class="alert-link">ログアウト</a>
					<div class="row">
<div style = "margin-bottom:20px"></div>
</div>
			</div>
		</header>
	</div>
<div class="row" >
 <div class="col-sm-4">
<div class="box1">
<h1>${month}月の収入割合グラフ</h1>
    <canvas id="myChart1" width="10px" height="10px"></canvas>
　　　　　　　　<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.bundle.js"></script>
    <script>
    var ctx = document.getElementById("myChart1");
    var array01=${subListIn};
    var array03=${subListInNm};
    console.log( array01 );

    var myChart = new Chart(ctx, {
      type: 'doughnut',
      data:  {
          labels: array01,
          datasets: [{
              backgroundColor: [
                  "#BB5179",
                  "#EAAF00",
                  "#58A27C",
                  "#3C00FF"
              ],
              data:array03
          }]
        },
        options: {
            title: {
                display: true,
                text: '科目 割合'
              }
            }
    });
    </script>
    </div>
    </div>



 <div class="col-sm-4">
    <div class="box2">
    <h1>${month}月の支出割合グラフ</h1>
    <canvas id="myChart2" width="10px" height="10px"></canvas>
　　　　　　　　<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.bundle.js"></script>
    <script>
    var ctx = document.getElementById("myChart2");
    var array02=${subListEx};
    var array04=${subListExNm};
    console.log( array02 );

    var myChart = new Chart(ctx, {
      type: 'doughnut',
      data:  {
          labels: array02,
          datasets: [{
              backgroundColor: [
                  "#BB5179",
                  "#EAAF00",
                  "#58A27C",
                  "#3C00FF",
                  "#ff00ff",
                  "#ff8c00",
                  "#00ffff",
                  "#00ff00"

              ],
              data: array04
          }]
        },
      options: {
          title: {
              display: true,
              text: '科目 割合'
            }
          }
    });
    </script>
</div>
</div>



 <div class="col-sm-4">
<div class="box3">
    <h1>${month}月の収支割合グラフ</h1>
    <canvas id="myChart3" width="10px" height="10px"></canvas>
　　　　　　　　<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.bundle.js"></script>
    <script>
    var ctx = document.getElementById("myChart3");
    var array05=${totalIn};
    var array06=${totalEx};
    console.log( array05 );
    console.log( array06 );

    var myChart = new Chart(ctx, {
      type: 'doughnut',
      data:  {
          labels: ["収入", "支出"],
          datasets: [{
              backgroundColor: [
                  "#BB5179",
                  "#EAAF00",

              ],
              data: [array05,array06]
          }]
        },
      options: {
          title: {
              display: true,
              text: '科目 割合'
            }
          }
    });
    </script>
<div style = "margin-bottom:30px"></div>
</div>
</div>
</div>

    <div class="row" >
 <div class="col-sm-6"  align=right>
<a href="HomeMonth" class="alert-link">戻る</a>
</div>
</div>

</body>
</html>