<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ユーザー新規登録</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
	<link href="css/UserRegist.css" rel="stylesheet" type="text/css"/>
</head>
<body>
<div class="head" align="right">
		<header>
			<div class="alert alert-dark" role="alert">
				${sessionScope.userInfo.name}さん
					<a href="Logout" class="alert-link">ログアウト</a>
					<div class="row">
<div style = "margin-bottom:20px"></div>
</div>
			</div>
		</header>
	</div>
	<div class="row">
<div style = "margin-bottom:90px"></div>
</div>
<div  align="center">
<b>ユーザー新規登録</b>
<div class="row">
<div style = "margin-bottom:25px"></div>
</div>
<div>
<font color="red">
<c:if test="${error != null}" >
	    <div class="alert alert-danger" role="alert">
		  ${error}
		</div>
	</c:if>
	</font>
	</div>
<div class="row">
<div style = "margin-bottom:25px"></div>
</div>
<form action="UserRegist" method=post>
<table>
<tr>
<td>ログインID</td>
<td></td>
<td><input type="text" name="registLoId"></td>
</tr>
<tr>
<td>パスワード</td>
<td></td>
<td><input type="password" name="registPass"></td>
</tr>
<tr>
<td>パスワード(確認)</td>
<td></td>
<td><input type="password" name="registPs"  ></td>
</tr>
<tr>
<td>ユーザー名</td>
<td></td>
<td><input type="text" name="registUsNm"></td>
</tr>
<tr>
<td>生年月日</td>
<td></td>
<td><input type="text" name="registBrDt" pattern=\d{4}-\d{2}-\d{2} placeholder="年-月-日" ></td>
</tr>
</table>
<div class="row">
<div style = "margin-bottom:30px"></div>
</div>
<div class="row" >
 <div class="col-sm-3">
<a href="Login" class="alert-link">戻る</a>
</div>
<div  class="col-sm-6">​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​
			<input type="submit" value="登録">
			</div>
			</div>
			</form>
</div>

</body>
</html>