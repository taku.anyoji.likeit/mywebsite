<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ログイン画面</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<link href="css/Login.css" rel="stylesheet" type="text/css" />
</head>
<body>
<form action="Login" method="post">
<div class="row">
<div style = "margin-bottom:90px"></div>
</div>
<div  align="center">
<div align="right">
			<a href="UserRegist" class="alert-link">ユーザー新規登録</a>
			</div>
			<div class="row">
<div style = "margin-bottom:90px"></div>
</div>
<p>ログイン画面</p>
<div class="row">
<div style = "margin-bottom:25px"></div>
</div>
<div>
<font color="red">
<c:if test="${error != null}" >
	    <div class="alert alert-danger" role="alert">
		  ${error}
		</div>
	</c:if>
	</font>
	</div>
<div class="row">
<div style = "margin-bottom:25px"></div>
</div>
<h3>ログイン画面　　　　<input type="text" name="loginId"></h3><br>
<h3>パスワード　　　　　<input type="password" name="loginPass"></h3><br>
<div class="row">
<div style = "margin-bottom:60px"></div>
</div>
<input type="submit"value="ログイン" >
</div>
</form>

</body>
</html>