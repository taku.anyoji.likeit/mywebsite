<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>収支登録内容編集</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
	<link href="css/SubjectEdit.css" rel="stylesheet" type="text/css" />
</head>
<body>
<div class="head" align="right">
		<header>
			<div class="alert alert-dark" role="alert">
				${sessionScope.userInfo.userName}さん
					<a href="Logout" class="alert-link">ログアウト</a>
					<div class="row">
<div style = "margin-bottom:20px"></div>
</div>
			</div>
		</header>
	</div>
	<div class="row">
<div style = "margin-bottom:90px"></div>
</div>
<div  align="center">
<b>収支登録内容編集</b>
<div class="row">
<div style = "margin-bottom:50px"></div>
</div>
<div>
<font color="red">
<c:if test="${error != null}" >
	    <div class="alert alert-danger" role="alert">
		  ${error}
		</div>
	</c:if>
	</font>
	</div>
<form action="SubjectEdit" method="post">
<table>
<tr>
<td>日付</td>
<td></td>
<td>
<input type="date" name="SubEdDate" >
<input type="hidden" name="SubEdUserId" value="${sessionScope.userInfo.userId}">
<input type="hidden" name="subEditId" value="${sessionScope.editId}">
</td>
</tr>
<tr>
<td>科目</td>
<td></td>
<td>
<select  name="SubjectEdName">
<c:forEach var="sub" items="${sessionScope.subList}">
<option value="${sub.subjectId}">${sub.subjectName}</option>
</c:forEach>
</select>
</td>
</tr>
<tr>
<td>金額</td>
<td></td>
<td><input type="number" name="money" pattern=”\d*”  value="${sessionScope.subEd.money}"  required >
</td>
</tr>
<tr>
<td>メモ</td>
<td></td>
<td><textarea name="memo" rows="2" cols="22" >${sessionScope.subEd.memo}</textarea>
</td>
</tr>
</table>
<div class="row">
<div style = "margin-bottom:30px"></div>
</div>
<div class="row" >
 <div class="col-sm-3">

<a href="DayDetail?year=${sessionScope.thisYear}&month=${sessionScope.thisMonth}&date=${sessionScope.thisDate}" class="alert-link">戻る</a>
</div>
<div  class="col-sm-6">​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​
			<input type="submit" value="更新">
			</div>
			</div>
			</form>
</div>

</body>
</html>