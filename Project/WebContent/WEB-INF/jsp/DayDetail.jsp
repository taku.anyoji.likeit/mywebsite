<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>収支詳細</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
	<link href="css/DayDetail.css" rel="stylesheet" type="text/css" />
</head>
<body>
<div class="head" align="right">
		<header>
			<div class="alert alert-dark" role="alert">
				${sessionScope.userInfo.userName}さん
					<a href="Logout" class="alert-link">ログアウト</a>
					<div class="row">
<div style = "margin-bottom:20px"></div>
</div>
			</div>
		</header>
	</div>
	<div class="row">
<div style = "margin-bottom:90px"></div>
</div>
<div  align="center">
<b>${thisYear}/${thisMonth}/${thisDate}の収支詳細</b>
<div class="row">
<div style = "margin-bottom:50px"></div>
</div>
		<table>
				<tr>
					<th>科目</th>
					<th>金額</th>
					<th>メモ</th>
					<th></th>
				</tr>
				<c:forEach var="i" items="${subList}"  >
				<tr>
<td> ${i.subjectName} </td>
<td>${i.money} </td>
<td>${i.memo} </td>
<td>
<a class="btn btn-primary" href="SubjectEdit?id=${i.id}&year=${thisYear}&month=${thisMonth}&date=${thisDate}">編集</a>
<a class="btn btn-danger" href="SubjectDelete?id=${i.id}&year=${thisYear}&month=${thisMonth}&date=${thisDate}">削除</a>
</td>
  </tr>
				</c:forEach>
  <tr>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</table>
<a href="HomeMonth" class="alert-link">戻る</a>


	</div>

</body>
</html>