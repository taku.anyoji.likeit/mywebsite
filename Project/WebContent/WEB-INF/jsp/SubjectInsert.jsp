<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
        <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>科目編集</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
	<link href="css/SubjectInsert.css" rel="stylesheet" type="text/css" />
</head>
<body>
<div class="head" align="right">
		<header>
			<div class="alert alert-dark" role="alert">
				${sessionScope.userInfo.userName}さん
					<a href="Logout" class="alert-link">ログアウト</a>
					<div class="row">
<div style = "margin-bottom:20px"></div>
</div>
			</div>
		</header>
	</div>
		<div class="row">
<div style = "margin-bottom:90px"></div>
</div>
<div  align="center">
<b>科目編集</b>
<div class="row">
<div style = "margin-bottom:50px"></div>
</div>

<div class="row">
<div  class="col-sm-4" align="right">
<table>
				<tr>
					<th>収支カテゴリ</th>
				</tr>
				<tr>
				<td rowspan="${listIns}">収入</td>
				</tr>
				</table>
				</div>
<div class="col-sm-2">
<table>
                <tr>
					<th>科目名称</th>
					<th></th>
				</tr>
                <c:forEach var="i" items="${subListIn}"  >
				<tr>
				<td>
				${i.subjectName}
				</td>
				<td>
				<a href="SubjectNameDelete?subjectId=${i.subjectId}" class="btn btn-danger">削除</a>
				</td>
				</tr>
				</c:forEach>
				</table>
</div>
<div  class="col-sm-2" align="right">
<table>
<tr>
					<th>収支カテゴリ</th>
				</tr>
				<tr>
				<td rowspan="${listExs}">支出</td>
				</tr>
				</table>
				</div>
<div class="col-sm-4" align="left">
<table>
                 <tr>
					<th>科目名称</th>
					<th></th>
				</tr>
<c:forEach var="i" items="${subListEx}"  >
				<tr>
				<td>
				${i.subjectName}
				</td>
				<td>
				<a href="SubjectNameDelete?subjectId=${i.subjectId}" class="btn btn-danger">削除</a>
				</td>
				</tr>
				</c:forEach>
				</table>
				</div>
				</div>
				<div class="row">
                <div style = "margin-bottom:50px"></div>
                </div>
                </div>
                 <div class="col-sm-3">
                 <a href="HomeMonth" class="alert-link">戻る</a>
                 </div>
                <div  class="text-center" >
				<a class="btn btn-success" href="SubjectNameInsert">新科目登録</a>
</div>
</body>
</html>