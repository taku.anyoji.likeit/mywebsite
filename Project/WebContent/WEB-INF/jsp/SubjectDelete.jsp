<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>登録内容削除</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
	<link href="css/SubjectDelete.css" rel="stylesheet" type="text/css" />
</head>
<body>
<div class="head" align="right">
		<header>
			<div class="alert alert-dark" role="alert">
				${sessionScope.userInfo.userName}さん
					<a href="Logout" class="alert-link">ログアウト</a>
					<div class="row">
<div style = "margin-bottom:20px"></div>
</div>
			</div>
		</header>
	</div>
	<div class="row">
<div style = "margin-bottom:90px"></div>
</div>
<div align="center">
<b>削除確認</b>
<div class="row">
<div style = "margin-bottom:50px"></div>
</div>
<form action="SubjectDetele" method="post">
<input type="hidden" name="deleteId" value="${subDelete.id}">
<table>
<tr>
<td>日付</td>
<td>${subDelete.createDate}</td>
</tr>
<tr>
<td>科目</td>
<td>${subDelete.subjectName}</td>
</tr>
<tr>
<td>金額</td>
<td>${subDelete.money}</td>
</tr>
<tr>
<td>メモ</td>
<td>${subDelete.memo}</td>
</tr>
<tr>
<td>
を本当に削除してもよろしいでしょうか。
</td>
</tr>
<tr>
<td>
<div align="center">
  <button type="button" onClick="history.back()'">戻る</button>　　　　　<input type="submit" value="OK">
  </div>
</td>
</tr>
  </table>

</form>

</div>

</body>
</html>